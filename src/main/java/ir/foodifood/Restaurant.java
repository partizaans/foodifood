package ir.foodifood;

import java.util.ArrayList;

public class Restaurant {
    public String name;
    String description;
    Location location;
    ArrayList<Food> menu = new ArrayList<>();

    public void add_food(Food food) {
        this.menu.add(food);
    }

    public Food get_food(String food_name) {
        for (Food f : this.menu)
            if (f.name.equals(food_name))
                return f;
        return null;
    }

    public float calculate_populations() {
        float sum = 0;
        float distance = 0;
        distance = (float) Math.sqrt(Math.pow(location.x, 2) + Math.pow(location.y, 2));
        for (Food food : menu)
            sum = sum + food.popularity;

        if (menu.size() == 0 || distance == 0) {
            return 0;
        }
        float popularity = (sum / menu.size()) / distance;
        return popularity;
    }

}
