package ir.foodifood;

import ir.foodifood.exceptions.DuplicateKey;

import java.util.ArrayList;

public class RestaurantRepo {
    private static ArrayList<Restaurant> items = new ArrayList<>();
    private ArrayList<RestaurantPopularity> all = new ArrayList<>();
    RestaurantPopularity r = new RestaurantPopularity();

    public void add_item(Restaurant restaurant) throws DuplicateKey {
        if (this.find_restaurant(restaurant.name) != null) {
            throw new DuplicateKey();
        }
        items.add(restaurant);
    }

    public void delete_all() {
        items = new ArrayList<>();
    }

    public ArrayList<Restaurant> find_all() {
        return items;
    }

    public Restaurant find_restaurant(String name) {
        for (Restaurant item : items)
            if (name.equals(item.name))
                return item;
        return null;
    }

    public static class RestaurantPopularity
    {
        public String restaurantName ;
        float popularity;
        public float get_popularity()
        {
            return popularity;
        }
    }

    public ArrayList<RestaurantPopularity> find_all_populariy() {
        make_popularity_list();
        return all;
    }

    public void make_popularity_list() {
        for (Restaurant item : items) {
            RestaurantPopularity r = new RestaurantPopularity();
            r.restaurantName = item.name;
            r.popularity = item.calculate_populations();
            all.add(r);
        }
    }


}
