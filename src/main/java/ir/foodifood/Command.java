package ir.foodifood;

public class Command {
    String name;
    Handler handler;

    public Command(String name, Handler handler) {
        this.name = name;
        this.handler = handler;
    }

    String handle(String data) throws Exception {
        return handler.handle(data);
    }
}
