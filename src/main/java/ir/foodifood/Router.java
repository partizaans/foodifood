package ir.foodifood;

import ir.foodifood.exceptions.BadParamsException;
import ir.foodifood.exceptions.NotFoundException;
import ir.foodifood.handlers.*;

import java.util.ArrayList;
import java.util.List;

public class Router {
    private ArrayList<Command> commands;

    public Router() {
        this.commands = new ArrayList<>();
        this.commands.add(new Command("addRestaurant", new AddRestaurantHandler()));
        this.commands.add(new Command("addFood", new AddFoodHandler()));
        this.commands.add(new Command("getRestaurants", new GetRestaurantsHandler()));
        this.commands.add(new Command("getRestaurant", new GetRestaurantHandler()));
        this.commands.add(new Command("getFood", new GetFoodHandler()));
        this.commands.add(new Command("addToCart", new AddToCartHandler()));
        this.commands.add(new Command("getCart", new GetCartHandler()));
        this.commands.add(new Command("finalizeOrder", new FinalizeOrderHandler()));
        this.commands.add(new Command("getRecommendedRestaurants", new GetRecommendedRestaurantsHandler()));
    }

    public String route(List<String> data) throws Exception {
        if (data.size() < 1 || data.size() > 2) {
            throw new BadParamsException();
        }
        for (Command command : commands) {
            if (command.name.equals(data.get(0))) {
                String data_param = null;
                if (data.size() == 2) {
                    data_param = data.get(1);
                }
                return command.handle(data_param);
            }
        }
        throw new NotFoundException();

    }
}
