package ir.foodifood;

import java.util.ArrayList;

public class Cart {
    static String restaurantName = null;
    private static ArrayList<CartItem> items = new ArrayList<>();

    public static boolean add_to_cart(CartItem c) {
        if (check_condition(c)) {
            if (restaurantName == null) {
                restaurantName = c.restaurantName;
            }

            if (restaurantName.equals(c.restaurantName)) {
                items.add(c);
                return true;
            }
        }
        return false;
    }

    public static boolean check_condition(CartItem c) {
        RestaurantRepo restaurant_repo = new RestaurantRepo();
        Restaurant r = restaurant_repo.find_restaurant(c.restaurantName);
        if (r != null) {
            Food f = r.get_food(c.foodName);
            if (f != null)
                return true;
        }
        return false;
    }

    public static ArrayList<CartItem> get_items() {
        return items;
    }

    public static boolean finalize_order() {
        if (items.size() != 0) {
            restaurantName = null;
            items.clear();
            return true;
        }
        return false;
    }
}

