package ir.foodifood;

public interface Handler {
    String handle(String data) throws Exception;
}
