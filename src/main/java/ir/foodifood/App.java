package ir.foodifood;

import com.google.gson.JsonSyntaxException;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        Router router = new Router();
        while (in.hasNext()) {
            String s = in.nextLine();
            List<String> data = Arrays.asList(s.split(" ", 2));
            try {
                System.out.println(router.route(data));
            } catch (JsonSyntaxException e) {
                System.out.println("BAD_PARAMS");
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
