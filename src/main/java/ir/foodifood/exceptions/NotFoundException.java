package ir.foodifood.exceptions;

public class NotFoundException extends Exception {
    public NotFoundException() {
        super("NOT_FOUND");
    }
}
