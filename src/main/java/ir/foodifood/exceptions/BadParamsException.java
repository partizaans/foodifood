package ir.foodifood.exceptions;

public class BadParamsException extends Exception {
    public BadParamsException() {
        super("BAD_PARAMS");
    }
}
