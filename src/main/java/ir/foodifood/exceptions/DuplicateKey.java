package ir.foodifood.exceptions;

public class DuplicateKey extends Exception {
    public DuplicateKey() {
        super("DUPLICATE_KEY");
    }
}
