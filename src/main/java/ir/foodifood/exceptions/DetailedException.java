package ir.foodifood.exceptions;

public class DetailedException extends Exception {
    String message;

    public DetailedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
