package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Handler;
import ir.foodifood.RestaurantRepo;
import ir.foodifood.exceptions.NotFoundException;

import java.util.HashMap;

public class GetRestaurantHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {

        var queried_data = new Gson().fromJson(data, HashMap.class);
        var restaurant = new RestaurantRepo().find_restaurant(queried_data.get("name").toString());
        if (restaurant == null) {
            throw new NotFoundException();
        }
        return new Gson().toJson(restaurant);
    }
}
