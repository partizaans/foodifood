package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Cart;
import ir.foodifood.CartItem;
import ir.foodifood.Handler;

import java.util.ArrayList;


public class GetCartHandler implements Handler {
    @Override
    public String handle(String data) {
        ArrayList<CartItem> items;
        items = Cart.get_items();
        ResponseHandler r = new ResponseHandler();
        r.make_reciept(items);
        return new Gson().toJson(r);
    }

    public class ResponseHandler {
        int number;
        ArrayList<String> foodNames = new ArrayList<>();

        public void make_reciept (ArrayList<CartItem> items) {
            for (int i = 0; i < items.size(); i++) {
                number = items.size();
                foodNames.add(items.get(i).foodName);
            }
        }
    }
}