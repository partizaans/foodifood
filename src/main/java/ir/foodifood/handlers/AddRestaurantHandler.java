package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Handler;
import ir.foodifood.Restaurant;
import ir.foodifood.RestaurantRepo;

public class AddRestaurantHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {
        Restaurant r = new Gson().fromJson(data, Restaurant.class);
        RestaurantRepo restaurant_repo = new RestaurantRepo();
        restaurant_repo.add_item(r);
        return "Success";
    }
}
