package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Cart;
import ir.foodifood.CartItem;
import ir.foodifood.Handler;
import ir.foodifood.exceptions.DetailedException;

import java.util.ArrayList;


public class FinalizeOrderHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {
        ArrayList<CartItem> items, temp = new ArrayList<>();
        items = Cart.get_items();
        for (CartItem item : items)
            temp.add(item);

        ResponseHandler r = new ResponseHandler();
        r.make_reciept(temp);
        if (Cart.finalize_order())
            return new Gson().toJson(r);

        throw new DetailedException("no food to finalize");
    }

    public class ResponseHandler {
        int number;
        ArrayList<String> foodNames = new ArrayList<>();

        public void make_reciept(ArrayList<CartItem> items) {
            for (int i = 0; i < items.size(); i++) {
                number = items.size();
                foodNames.add(items.get(i).foodName);
            }
        }
    }
}