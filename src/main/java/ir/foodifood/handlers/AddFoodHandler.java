package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Food;
import ir.foodifood.Handler;
import ir.foodifood.RestaurantRepo;
import ir.foodifood.exceptions.NotFoundException;


public class AddFoodHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {
        Food f = new Gson().fromJson(data, Food.class);
        var restaurant = new RestaurantRepo().find_restaurant(f.restaurantName);
        if (restaurant == null) {
            throw new NotFoundException();
        }
        restaurant.add_food(f);
        return "Success";


    }
}
