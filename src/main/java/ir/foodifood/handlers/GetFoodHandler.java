package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Food;
import ir.foodifood.Handler;
import ir.foodifood.Restaurant;
import ir.foodifood.RestaurantRepo;
import ir.foodifood.exceptions.NotFoundException;

import java.util.HashMap;

public class GetFoodHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {
        var queried_data = new Gson().fromJson(data, HashMap.class);
        Restaurant restaurant = new RestaurantRepo().find_restaurant(queried_data.get("restaurantName").toString());

        if (restaurant == null) {
            throw new NotFoundException();
        }
        Food f = restaurant.get_food(queried_data.get("foodName").toString());

        if (f == null) {
            throw new NotFoundException();
        }
        return new Gson().toJson(f);
    }
}
