package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Handler;
import ir.foodifood.Restaurant;
import ir.foodifood.RestaurantRepo;

import java.util.ArrayList;

public class GetRestaurantsHandler implements Handler {
    @Override
    public String handle(String data) {
        ArrayList<String> restaurant_names = new ArrayList<>();
        for (Restaurant r : new RestaurantRepo().find_all()) {
            restaurant_names.add(r.name);
        }
        return new Gson().toJson(restaurant_names);
    }
}
