package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Cart;
import ir.foodifood.CartItem;
import ir.foodifood.Handler;


public class AddToCartHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {
        CartItem c = new Gson().fromJson(data, CartItem.class);
        if (!Cart.add_to_cart(c)) {
            return "Failed";
        }
        return "Added to cart!";

    }
}