package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.Handler;
import ir.foodifood.RestaurantRepo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class GetRecommendedRestaurantsHandler implements Handler {
    @Override
    public String handle(String data) throws Exception {
        ArrayList<RestaurantRepo.RestaurantPopularity> all;
        ArrayList<RestaurantRepo.RestaurantPopularity> response = new ArrayList<>();
        RestaurantRepo r = new RestaurantRepo();
        all = r.find_all_populariy();

        RestaurantRepo.RestaurantPopularity e = null;
        if (all.size() > 2) {
            for (int i = 0; i < 3; i++) {
                if (e != null)
                    all = delete_maximum(e, all);

                e = Collections.max(all, Comparator.comparingDouble(RestaurantRepo.RestaurantPopularity::get_popularity));
                response.add(e);
            }
        }
        return new Gson().toJson(response);
    }

    public ArrayList<RestaurantRepo.RestaurantPopularity> delete_maximum(RestaurantRepo.RestaurantPopularity r, ArrayList<RestaurantRepo.RestaurantPopularity> all) {
        for (int i = 0; i < all.size(); i++) {
            if (all.get(i).restaurantName == r.restaurantName)
                all.remove(i);
        }
        return all;
    }
}