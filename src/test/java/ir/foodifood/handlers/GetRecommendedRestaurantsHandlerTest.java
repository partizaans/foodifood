package ir.foodifood.handlers;

import com.google.gson.Gson;
import ir.foodifood.RestaurantRepo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

class GetRecommendedRestaurantsHandlerTest {

    @BeforeEach
    void setUp() throws Exception {
        String stringedR1 = new String(Files.readAllBytes(Paths.get("src/test/java/ir/foodifood/handlers/test_data/restaurants/1.json")));
        String stringedR2 = new String(Files.readAllBytes(Paths.get("src/test/java/ir/foodifood/handlers/test_data/restaurants/2.json")));
        String stringedR3 = new String(Files.readAllBytes(Paths.get("src/test/java/ir/foodifood/handlers/test_data/restaurants/3.json")));
        String stringedR4 = new String(Files.readAllBytes(Paths.get("src/test/java/ir/foodifood/handlers/test_data/restaurants/4.json")));


        new AddRestaurantHandler().handle(stringedR1);
        new AddRestaurantHandler().handle(stringedR2);
        new AddRestaurantHandler().handle(stringedR3);
        new AddRestaurantHandler().handle(stringedR4);
    }

    @AfterEach
    void tearDown() {
        new RestaurantRepo().delete_all();
    }

    @Test
    void testWhenThereAreEnoughRestaurant() throws Exception {
        String response = new GetRecommendedRestaurantsHandler().handle("");
        var converted_result = new Gson().fromJson(response, RestaurantRepo.RestaurantPopularity[].class);
        Assertions.assertEquals(3, converted_result.length, "It should be 3 based on requirements");
        Assertions.assertEquals("R4", converted_result[0].restaurantName, "R4 has highest popularity");
        Assertions.assertEquals("R1", converted_result[1].restaurantName, "R1 has middle popularity and middle distance");
        Assertions.assertEquals("R2", converted_result[2].restaurantName, "R2 has better score than R3 (0)");

    }

    @Test
    void testWhenThereIsNoRestaurant() throws Exception {
        new RestaurantRepo().delete_all();
        String response = new GetRecommendedRestaurantsHandler().handle("");
        Assertions.assertEquals("[]", response, "It should return empty list gently");
    }

}