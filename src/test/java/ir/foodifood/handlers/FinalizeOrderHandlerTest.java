package ir.foodifood.handlers;

import ir.foodifood.*;
import ir.foodifood.exceptions.DetailedException;
import ir.foodifood.exceptions.DuplicateKey;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FinalizeOrderHandlerTest {
    FinalizeOrderHandler handler;
    Restaurant test_restaurant;
    Food test_food;

    @BeforeEach
    void setUp() {
        this.handler = new FinalizeOrderHandler();
        test_restaurant = new Restaurant();
        test_restaurant.name = "Test Restaurant";

        test_food = new Food();
        test_food.restaurantName = test_restaurant.name;
        test_food.name = "Test Food";
        test_restaurant.add_food(test_food);
        try {
            new RestaurantRepo().add_item(test_restaurant);
        } catch (DuplicateKey duplicateKey) {
            fail();
        }
    }

    @AfterEach
    void tearDown() {
        new RestaurantRepo().delete_all();
    }

    @Test
    void testWhenNoFoodExistsInCart() {
        assertThrows(DetailedException.class, () -> this.handler.handle(null));
    }

    void populateCart() {
        CartItem testCartItem = new CartItem();
        testCartItem.restaurantName = test_restaurant.name;
        testCartItem.foodName = test_food.name;
        Cart.add_to_cart(testCartItem);
    }

    @Test
    void testDoesNotThrows() {
        this.populateCart();
        assertDoesNotThrow(() -> this.handler.handle(""));
    }

    @Test
    void testReturnsCorrectResponse() throws Exception {
        this.populateCart();
        String response = this.handler.handle("");
        assertEquals("{\"number\":1,\"foodNames\":[\"Test Food\"]}", response);
    }

    @Test
    void testCartGetsEmptyAfterFinalizing() throws Exception {
        this.populateCart();
        this.handler.handle("");
        assertEquals(0, Cart.get_items().size());
    }
}